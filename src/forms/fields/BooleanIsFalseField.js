import React from 'react';
import TrueIcon from '@material-ui/icons/Done';

const BooleanIsFalseField = ({ source, record = {} }) => <span>{record[source] === false ?<TrueIcon/> : null }{record[source]}</span>;

export default BooleanIsFalseField;