import React from "react";
import {Pagination} from 'react-admin';

export const ListPagination = props => <Pagination rowsPerPageOptions={[10, 50, 100, 1000]} {...props} />
