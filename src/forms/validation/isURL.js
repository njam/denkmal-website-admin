export const isURL = value => {
    return value && !/^(?:\w+:)?\/\/([^\s.]+\.\S{2}|localhost[:?\d]*)\S*$/.test(value) ? 'Use a valid URL starting with http:// or https://' : undefined;
}