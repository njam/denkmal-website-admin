export const isReviewPendingStyle = {
    boxShadow: `inset 0px 0px 0px 5px rgba(208, 215, 247, 1.0)`
};

export const isPromotedStyle = {
    boxShadow: `inset 0px 0px 0px 5px rgba(255, 215, 0, 0.5)`
};