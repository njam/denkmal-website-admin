export const getRegionsFromState = (state) => {
    if (state.admin && state.admin.resources && state.admin.resources.region && state.admin.resources.region.list.loadedOnce) {
        return Object.values(state.admin.resources.region.data);
    }
    if (state.regions) {
        return Object.values(state.regions);
    }
    return null;
}