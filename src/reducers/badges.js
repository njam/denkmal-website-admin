export default (previousState = {}, { type, badges, regionId }) => {
    if (type === 'FETCHED_BADGES') {
        const newState = {
            ...previousState
        };
        newState[regionId] = badges;
        return newState;
    }
    return previousState;
}