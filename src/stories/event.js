import React from 'react';
import { storiesOf } from '@storybook/react';
import {EventListItem} from "../dashboard/EventListItem";
import {EventList} from "../dashboard/EventList";
import {EventDay} from "../dashboard/EventDay";
import {EventDays} from "../dashboard/EventDays";
import {BrowserRouter} from 'react-router-dom';
import moment from 'moment-timezone';

const myEvent = {
    id: "123",
    description: "A super event",
    from: "22.1.2019",
    venue: {
        name: "The venue"
    }
};
const anotherEvent = {
    id: "124",
    description: "Another event",
    from: "13.3.2018",
    venue: {
        name: "Super venue"
    }
};

storiesOf('EventListItem', module)
    .add('event', () => <EventListItem event={myEvent}/>);


storiesOf('EventList', module)
    .add('event list', () => <EventList events={[myEvent, anotherEvent]}/>);

storiesOf('EventDay', module)
    .add('event day', () => <EventDay events={[myEvent, anotherEvent]} eventDay={moment().tz("Australia/Sydney")}/>);

const eventDays = [
    {
        eventDay: moment().tz("America/Los_Angeles"),
        events: [myEvent, anotherEvent]
    },
    {
        eventDay: moment().tz("America/Los_Angeles").add(1, 'days'),
        events: [myEvent]
    },
    {
        eventDay: moment().tz("America/Los_Angeles").add(2, 'days'),
        events: [anotherEvent]
    }
];

storiesOf('EventDays', module)
    .add('event days', () => <BrowserRouter><EventDays eventDays={eventDays}/></BrowserRouter>);

